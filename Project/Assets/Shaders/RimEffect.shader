﻿/*
 *	Author: Marius Varga & Luke Taylor
 *  Purpose: Custom shader to give a specular with atmosphere intergration, utilising a custom lighting model.
*/

Shader "Custom/RimEffect" 
{
	Properties 
	{
		_Color("MainColor", Color) = (1, 0, 0, 1)
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_BumpMap("Normal Map", 2D) = "bump" {}
		_SpecColor("SpecularColor", Color) = (1, 1, 1, 1)
		_Shininess("SpecPower", Range(0, 1)) = 0.5
		_Glossiness("GlossPower", Range(0,1)) = 0.5
		_AtmColor("AtmColor", Color) = (1, 1, 1, 1) 
		_AtmPower("AtmPower", Range(0,1)) = 0.5
	}
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Ramp
		//#pragma 2.0

		float4 _Color;
		sampler2D _MainTex;
		sampler2D _BumpMap;
		float _Shininess;
		float _Glossiness;
		fixed _RimPower;
		fixed4 _RimColor;
		float4 _AtmColor;
		float _AtmPower;

		struct Input 
		{
			float2 uv_MainTex;
			float2 uv_BumpMap;
			float3 viewDir;
		};

		half4 LightingRamp (SurfaceOutput s, half3 lightDir, half3 viewDir, half atten)
		{
			//Light direction, using the normal to calculate the product between the normal of Verts and normal of LightDir
			float NdotL = dot(s.Normal, normalize(lightDir));

			//Atmosphere Calc
			float NdotE = dot(s.Normal, normalize(viewDir));

			//Specular calc
			half3 halfVector = normalize(lightDir + viewDir);
			fixed NdotH = max(0, dot(s.Normal, halfVector));
			fixed spec = pow(NdotH, s.Specular * 128) * s.Gloss;

			//Diff
			float diff = saturate(NdotL);

			//RimLight
			fixed rimLight = 1.0 - NdotE;
			rimLight = pow(rimLight, _RimPower) * NdotH;

			//Creating a color to return and display
			float4 c;

			//Albedo gets the Material Color and LightColor gets the single Directional light color adding the rim light and color
			c.rgb = s.Albedo * _LightColor0.rgb * diff + _SpecColor.rgb * spec + rimLight * _RimColor.rgb;
			c.a = s.Alpha;

			return c;
		}

		void surf (Input IN, inout SurfaceOutput o) 
		{
			half4 c = tex2D (_MainTex, IN.uv_MainTex);
			float3 bump = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
			o.Normal = bump.rgb;

			o.Albedo = c.rgb * _Color.rgb;
			o.Alpha = c.a;

			//Specular
			o.Specular = _Shininess;
			o.Gloss = _Glossiness;

			//"Atmosphere" type light, utilising the Hollywood model
			half atm = 1 - saturate(dot(normalize(IN.viewDir), o.Normal));
			o.Emission = _AtmColor.rgb * pow(atm, _AtmPower);
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
