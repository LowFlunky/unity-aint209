﻿using UnityEngine;
using System.Collections;

public class LightOnExplosion : MonoBehaviour {
    /*
     * Author: Luke Taylor
     * Purpose: On an explosion being created, a light will fade out over a few seconds.
     */ 
	
    // On creation start the Coroutine
	void Start () {
		StartCoroutine("LightOff");
	}
	
    // Coroutine loops through every 0.5 seconds, 7  times, and reduces intesity of the light before destorying it to remove clutter.
	IEnumerator LightOff()
	{
		for (int i = 0; i < 7; i++) 
		{
			yield return new WaitForSeconds (0.5f);
			light.intensity -= 1;
		}
		Destroy(light);
	}
}
