﻿using UnityEngine;
using System.Collections;

public class FollowPlayer : MonoBehaviour {

    /*
     * Author: Luke Taylor
     * Purpose: Enemy will choose one of the living players and follow them.
     */

    // The speed of movement, changes depending on enemy type via Prefab
    public float moveSpeed = 3;
    // The rotation speed for turning to face the target
    public float rotationSpeed = 3;

    // The chosen target to follow
    private Transform target;
    // Transform used for movement
    private Transform myTransform;

    //On Initilisation this randomly chooses which player to chase, and checks if their health is higher than 0, if both are dead they do not move.
	void Start () 
    {
       myTransform = transform;
        // Random chance of player 1 or 2
       int i = Random.Range(0, 10);
       if (i < 5)
         {
            if (Gui.player1Health > 0)
                {
                    target = GameObject.FindWithTag("Player").transform;
                }
                else if (Gui.player2Health > 0)
                {
                    target = GameObject.FindWithTag("Player2").transform;
                }
            }
            else
            {
                if (Gui.player2Health > 0)
                {
                    target = GameObject.FindWithTag("Player2").transform;
                }
                else if (Gui.player1Health > 0)
                {
                    target = GameObject.FindWithTag("Player").transform;
                }
            }
	}
	
    //Late Update used to ensure that players will get followed after the enemy spawns, using Update had a chance that enemies would not start following for some reason.
    //This script uses the assigned target, checks their health and follows them. 
	void LateUpdate () 
    {
        if (Gui.player1Health <= 0)
        {
            target = GameObject.FindWithTag("Player2").transform;
        }

        if (Gui.player2Health <= 0)
        {
            target = GameObject.FindWithTag("Player").transform;
        }
            //Rotate towards player
        if (Gui.player1Health > 0 || Gui.player2Health > 0)
        {
            myTransform.rotation = Quaternion.Slerp(myTransform.rotation, Quaternion.LookRotation(target.position - myTransform.position), rotationSpeed * Time.deltaTime);

            //Move to Player
            myTransform.position += myTransform.forward * moveSpeed * Time.deltaTime;
        }
	}
   
}
