﻿using UnityEngine;
using System.Collections;

public class KeyboardControl : MonoBehaviour {

    /*
     * Author: Luke Taylor
     * Purpose: Moving the player via keyboard
     */ 

    //Player speed
    private float speed;

    //Players speed is set at start
	void Start () {
        speed = 10;
	}
	
    //While the players health is above 0, the input manager will detect the inputs and move the character in the correct direction, smoothing this with deltaTime.
	void Update () 
	{
        if (Gui.player1Health > 0)
        {
            // Translations are used for movement.
            float translation = Input.GetAxis("Vertical") * speed;
            float translation2 = Input.GetAxis("Horizontal") * speed;
            translation *= Time.deltaTime;
            translation2 *= Time.deltaTime;
            transform.Translate(0, 0, translation);
            transform.Translate(translation2, 0, 0);
        }
	}
}
