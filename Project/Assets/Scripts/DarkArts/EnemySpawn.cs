﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemySpawn : GameEntity
{
    /*
     * Author: Luke Taylor & Utilising Richard Weeks Pooling system
     * Purpose: Spawn Enemeies in a defined area, control wave amounts and count enemies for testing
     */

    // GameObject where enemies will spawn between
    [SerializeField]
    private GameObject spawnLocation;
    [SerializeField]
    private GameObject spawnLocation2;

    //Enemy counter for each wave
    public static int numberOfEnemies;
    //Array of all enemy prefabs
    public GameObject[] enemyPrefabs;

    // Wave counter for GUI
    [HideInInspector]
    public static int wave;
    // Wave handler
    [HideInInspector]
    public static bool waveDelay;

    //Call Initialise on start
    void Start()
    {
        Initialise();
    }

    public override void Initialise()
    {
        base.Initialise();

        //Set all numbers to their base set
        waveDelay = false;
        wave = 0;
        numberOfEnemies = 0;
        StartCoroutine("WaveDelay");
    }
    
    //Utilising a later frame check to count the number of enemies.
    void Update()
    {
        Debug.Log(numberOfEnemies);
        //Spawn script
        if (numberOfEnemies == 0 && waveDelay == false)
        {
            StartCoroutine("WaveDelay");
        }
    }

    //Handles the amount of enemies to spawn dependent on wave number
    void SpawnManager()
    {
        if (wave < 5)
        {
            for (int i = 0; i < 10; i++)
            {
                int randomEnemy = Random.Range(0, enemyPrefabs.Length);
                numberOfEnemies++;
                Enemy.Create(this, enemyPrefabs[randomEnemy],spawnLocation,spawnLocation2);
            }
        }
        else if (wave >= 5 && wave < 10)
        {
            for (int i = 0; i < 20; i++)
            {
                int randomEnemy = Random.Range(0, enemyPrefabs.Length);
                numberOfEnemies++;
                Enemy.Create(this, enemyPrefabs[randomEnemy], spawnLocation, spawnLocation2);
            }
        }
        else if (wave >= 10 && wave < 15)
        {
            for (int i = 0; i < 30; i++)
            {
                int randomEnemy = Random.Range(0, enemyPrefabs.Length);
                numberOfEnemies++;
                Enemy.Create(this, enemyPrefabs[randomEnemy], spawnLocation, spawnLocation2);
            }
        }
        else if (wave > 15)
        {
            for(int i = 0; i < 40; i++)
            {
                int randomEnemy = Random.Range(0, enemyPrefabs.Length);
                numberOfEnemies++;
                Enemy.Create(this, enemyPrefabs[randomEnemy], spawnLocation, spawnLocation2);
            }
        }
    }

    //Spawn delay between waves
    IEnumerator WaveDelay()
    {
        waveDelay = true;
        if (wave > 9)
        {

            yield return new WaitForSeconds(2);
            SpawnManager();
            wave++;
            waveDelay = false;
        }
        else
        {
            yield return new WaitForSeconds(5);
            SpawnManager();
            wave++;
            waveDelay = false;
        }
        
    }
}
