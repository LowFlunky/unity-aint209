﻿using UnityEngine;
using System.Collections;

public class Player : GameEntity
{

    /*
     * Author: Luke Taylor & utilising Richard Weeks' Pooling code
     * Purpose: Gives the Players the ability to shoot in difference directions and creates an AOE effect on spacebar key press, also allows 
     * for players to share health, and communicates to the GUI their current health levels.
     */

    //The bullet used for fireing
    public GameObject bulletPrefab;
    // Gunshot audio clip
    public AudioClip gunshot;
    // AOE particle effect
    public ParticleSystem explosion;

    // Freezes controls on start, Hidden in Inspector
    [HideInInspector]
    public static bool start = false;
    // Static Boolean used for AoE destruction, Hidden in Inspector
    [HideInInspector]
    public static bool aoeActive = false;
    // Used for testing speed values for bullets
    private float speed = 10.0f;
    // Health share Bool
    private bool shareHealth = false;
    // timer to keep players from spamming AoE
    private bool aoeCooldown = false;
    // Component to find the light to edit on death
    private Component[] children;

    // Assign all the child lights Unity can find to the children array then set each light to intesity 8 on start.
    void Start()
    {
        children = GetComponentsInChildren<Light>();
        foreach (Light light in children)
        {
            light.intensity = 8;
        }
    }

    //Overriding the inherited initalise method but currently not used.
    public override void Initialise()
    {
        base.Initialise();
    }

    //Update checks the current player and calls the appropriate controls methods for each
    void Update()
    {
        if (start == true)
        {
            if (this.CompareTag("Player"))
            {
                if (Gui.player1Health > 0)
                    Player1Controls();
            }
            else
            {
                if (Gui.player2Health > 0)
                    Player2Controls();
            }
        }
        lightHealth();
    }

    //Instantiates the AoE effect at the players position
    void GoBoom()
    {
        if (aoeCooldown == false)
        {
            Instantiate(explosion, transform.position, transform.rotation);
            aoeActive = true;
            aoeCooldown = true;
            StartCoroutine("AoE");
        }
    }

    //Script to lower the players light as their health goes down. Changed from a massive if/else statement to a health equivilancy assignment
    void lightHealth()
    {
        if (this.CompareTag("Player"))
        {
            foreach (Light light in children)
            {
                light.intensity = Gui.player1Health;
            }
        }
        else
        {
            foreach (Light light in children)
            {
                light.intensity = Gui.player2Health;
            }
        }
      
    }

    //Ensures that health will go up, and down, in increments after checking which player this is
    void OnTriggerStay(Collider otherPlayer)
    {
        if (this.CompareTag("Player"))
        {
            if (shareHealth == true && otherPlayer.gameObject.CompareTag("Player2"))
            {
                Gui.player2Health += 1;
                Gui.player1Health -= 1;
                shareHealth = false;
            }
        }
        else
        {
            if (shareHealth == true && otherPlayer.gameObject.CompareTag("Player"))
            {
                Gui.player1Health += 1;
                Gui.player2Health -= 1;
                shareHealth = false;
            }
        }
    }

    //Health sharing script
    void GiveHealth()
    {
        if (this.CompareTag("Player"))
        {
            if (Gui.player1Health > 1)
            {
                shareHealth = true;
                StartCoroutine("SplitHealth");
            }
        }
        else
        {
            if (Gui.player2Health > 1)
            {
                shareHealth = true;
                StartCoroutine("SplitHealth");
            }
        }
    }

    //Time used for incremental health
    IEnumerator SplitHealth()
    {
        yield return new WaitForSeconds(0.5f);
        shareHealth = false;
    }

    // Used to only allow the AoE attack to be used every 5 seconds, and to make sure that it can only hit enemies for 1 second.
    IEnumerator AoE()
    {
        yield return new WaitForSeconds(1);
        aoeActive = false;
        yield return new WaitForSeconds(5);
        aoeCooldown = false;
    }

    //Both AoE's have been disabled due to a bug in pooling that appears to destroy objects if too many are killed at once, causing an error in counting due to them being removed as they are being reused.
    //Player 1 controls
    private void Player1Controls()
    {
        if (Input.GetButtonDown("Fire1"))
            FireUp();
        if (Input.GetButtonDown("Fire2"))
            FireDown();
        if (Input.GetButtonDown("Fire3"))
            FireLeft();
        if (Input.GetButtonDown("Fire4"))
            FireRight();
        //if (Input.GetKeyDown("space"))
           // GoBoom();
        if (Input.GetKeyDown("f"))
            GiveHealth();
        if (Input.GetKeyDown("escape"))
            Application.Quit();
    }

    //Player 2 controls
    private void Player2Controls()
    {
        if (Input.GetButtonDown("Fire1Joy"))
            FireUp();
        if (Input.GetButtonDown("Fire2Joy"))
            FireDown();
        if (Input.GetButtonDown("Fire3Joy"))
            FireLeft();
        if (Input.GetButtonDown("Fire4Joy"))
            FireRight();
        //if (Input.GetButtonDown("Explosion"))
            //GoBoom();
        if (Input.GetButtonDown("Health"))
            GiveHealth();
    }

    //Fire directions, each direction assigneds a position on the correct side of the player object and adds the appropriate force
    void FireUp()
    {
        Vector3 newPos = new Vector3(transform.position.x, transform.position.y, transform.position.z + 1);
        Vector3 force = new Vector3(0, 0, 200 * speed);
        audio.PlayOneShot(gunshot);
        Bullet.Create(this, bulletPrefab, newPos, force);
    }

    void FireDown()
    {
        Vector3 newPos = new Vector3(transform.position.x, transform.position.y, transform.position.z - 1);
        Vector3 force = new Vector3(0, 0, -200 * speed);
        audio.PlayOneShot(gunshot);
        Bullet.Create(this, bulletPrefab, newPos, force);
    }

    void FireLeft()
    {
        Vector3 newPos = new Vector3(transform.position.x - 1, transform.position.y, transform.position.z);
        Vector3 force = new Vector3(-200 * speed, 0, 0);
        audio.PlayOneShot(gunshot);
        Bullet.Create(this, bulletPrefab, newPos, force);
    }

    void FireRight()
    {
        Vector3 newPos = new Vector3(transform.position.x + 1, transform.position.y, transform.position.z);
        Vector3 force = new Vector3(200 * speed, 0, 0);
        audio.PlayOneShot(gunshot);
        Bullet.Create(this, bulletPrefab, newPos, force);
    }

    //Returns this objects position when called
    public Vector3 GetTransform()
    {
        return transform.position;
    }
}
