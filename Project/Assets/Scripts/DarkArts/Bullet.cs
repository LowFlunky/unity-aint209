﻿using UnityEngine;
using System.Collections;

public class Bullet : GameEntity
{
    /*
     * Author: Luke Taylor, utilising Richard Weeks' Pooling code
     * Purpose: Create a new, or reuse an old, bullet that will be removed by the object pooling manager after 1 second or when colliding with an enemy.
     */

    //Force and position variables assigned by the direction of fire in the Player script.
    private Vector3 spawnPos;
    private Vector3 spawnForce;
    //The "Parent" Game Entity to be used when assigning the heirachy
    private GameEntity entityParent;


    //Create a pooled object instance, with the force and transform.
    public static void Create(GameEntity Entity, GameObject Prefab, Vector3 position, Vector3 force)
    {
        //Create an object from the prefab passed in
        GameObject NewBullet = ObjectPooler.Create(Prefab);

        //Create an entity of type Bullet with all of the prefabs components.
        Bullet BulletEntity = NewBullet.GetComponent<Bullet>();

        //Assign the parent for the hiearchy view
        BulletEntity.entityParent = Entity;
        //Set transform, force and initilaise the bullet.
        BulletEntity.SetTransform(position);
        BulletEntity.SetForce(force);
        BulletEntity.Initialise();
    }

    //Set Force.
    public void SetForce(Vector3 force)
    {
        spawnForce = force;
    }

    //Set Transform.
    public void SetTransform(Vector3 position)
    {
        spawnPos = position;
    }

    //Ensure Initilisation is called.
    void Start()
    {
        Initialise();
    }

    //On Intilisation set the position and force, called by the fire direction, and start the bullets life timer.
    public override void Initialise()
    {
        base.Initialise();
        StartCoroutine("DestroyAfterTime");
        //Ensure that its "current" velocity and angular velocity are 0 else bullets appear drunk when reused.
        this.rigidbody.velocity = Vector3.zero;
        this.rigidbody.angularVelocity = Vector3.zero;
        transform.position = spawnPos;
        this.rigidbody.AddForce(spawnForce);
    }

    //A second after the bullet has been fired, the trigger death function is called
    IEnumerator DestroyAfterTime()
    {
        yield return new WaitForSeconds(1);
        TriggerDeath();
    }

    //When the Tag of the collided object is an enemy, put this enemy on the closed list for reuse.
    void OnCollision(Collision col)
    {
        if (col.gameObject.CompareTag("Enemy"))
        {
            TriggerDeath();
            //EnemySpawn.change = true;
        }
    }
}
