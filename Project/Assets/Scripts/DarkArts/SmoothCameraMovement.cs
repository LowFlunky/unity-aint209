﻿using UnityEngine;
using System.Collections;

public class SmoothCameraMovement : MonoBehaviour 
{

    /*
     * Author: Luke Taylor, using code found via Stack Overflow and Yahoo answers to assist with Raycasting, checkpoint arrays and the view pos check
     * Purpose: Smooth the camera movement to follow player, if a boundary is blocking the view of the camera it will raise up to keep a line of sight, usefull if at the bottom of the map.
     */

    [Tooltip("The Player GameObject you wish to follow")]
    [SerializeField]
    private GameObject playerToFollow;

    // Players transform position
    private Transform player;
    // Speed of smoothing
	private float smooth = 1.5f;
    // Used to smooth the relative camera position
	private Vector3 relCameraPos;
    // Magnitude of smoothing
	private float relCameraPosMag;
    // New position to move the camera
	private Vector3 newpos;

    /*
     * Finds the player, finds its position relative to the player position and assigns the magnitude
     */ 
	void Awake()
	{
        player = playerToFollow.transform;
		relCameraPos = transform.position - player.position;
		relCameraPosMag = relCameraPos.magnitude - 0.5f;
	}


    /*
     * Assigns checkpoints for the camera to move to depending whether it can see directly to the player via ViewPosCheck, if it can see the player from those checkpoints 
     * it will take the first one which is true and move to it. Obviously standardPos is that which is defined at start, each following checkpoint is it being raised higher.
     */ 
	void FixedUpdate()
	{
		Vector3 standardPos = player.position + relCameraPos;
		Vector3 abovePos = player.position + Vector3.up * relCameraPosMag;
		Vector3[] checkpoints = new Vector3[5];
		checkpoints [0] = standardPos;
		checkpoints [1] = Vector3.Lerp (standardPos, abovePos, 0.25f);
		checkpoints [2] = Vector3.Lerp (standardPos, abovePos, 0.5f);
		checkpoints [3] = Vector3.Lerp (standardPos, abovePos, 0.75f);
		checkpoints [4] = abovePos;

		for (int i = 0; i < checkpoints.Length; i++) 
		{
			if(ViewingPosCheck(checkpoints[i]))
			{
				break;
			}
		}
        // Smooths movement to the new position so the camera does not stutter
		transform.position = Vector3.Lerp (transform.position, newpos, smooth * Time.deltaTime);
		SmoothLookAt ();
	}

    // Raycast used to check if the camera can fire a Ray directly at the player without any obstruction.
	bool ViewingPosCheck(Vector3 checkPos)
	{
		RaycastHit hit;

		if(Physics.Raycast(checkPos, player.position - checkPos, out hit, relCameraPosMag))
			if(hit.transform != player)
		{
			return false;
		}

		newpos = checkPos;
		return true;
	}

    // Smooth looking at player, this is most noticable when moving left or right, or when an object blocks a direct LoS
	void SmoothLookAt()
	{
		Vector3 relPlayerPosition = player.position - transform.position;
		Quaternion lookAtRotation = Quaternion.LookRotation (relPlayerPosition, Vector3.up);
		transform.rotation = Quaternion.Lerp (transform.rotation, lookAtRotation, smooth * Time.deltaTime);
	}
}
