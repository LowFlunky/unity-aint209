﻿using UnityEngine;
using System.Collections;

public class Enemy : GameEntity {

    /*
     * Author: Luke Taylor & Utilising Richard Weeks Pooling code
     * Purpose: Enemy collides with Player, lowers health and spawns an effect on its position..
     */

    // Particle System for death effect
    [Tooltip("Explosion effect when destroyed")]
    public ParticleSystem boom;

    protected GameEntity entityParent;

    // Number of enemies counter, used for testing and hidden in Editor.
    //[SerializeField]
    //public static int numberOfEnemies;
    // Boolean used to stop content being left over during the game closing. Hidden in Editor.
    [HideInInspector]
    public static bool isQuitting = false;


    public static void Create(GameEntity Entity, GameObject Prefab, GameObject enemySpawnLocation, GameObject enemySpawnLocation2)
    {
        //Create an object from the prefab passed in
        GameObject newEnemy = ObjectPooler.Create(Prefab);
        //Create an entity of type Bullet with all of the prefabs components.
        Enemy enemyEntity = newEnemy.GetComponent<Enemy>();
        //Assign the parent for the hiearchy view
        enemyEntity.entityParent = Entity;
        enemyEntity.transform.position = new Vector3((Random.Range(enemySpawnLocation.transform.position.x, enemySpawnLocation2.transform.position.y)), enemySpawnLocation.transform.position.y, enemySpawnLocation.transform.position.z);
        //Set transform, force and initilaise the bullet.
        enemyEntity.Initialise();
    }

    //On creation or on Awake call Initilise method
    void Start()
    {
        Initialise();
    }

    void Awake()
    {
        Initialise();
    }
    
    //Intalise through parents then reenable the collider and render
    public override void Initialise()
    {
 	    base.Initialise();
    }

    //Method to determine what to do based on collided object
	void OnCollisionEnter(Collision collision)
	{
        if (collision.gameObject.CompareTag("Player"))
        {
            Gui.player1Health -= 1;
            StartCoroutine("HitDelay");

        }
        else if (collision.gameObject.CompareTag("Player2"))
        {
            Gui.player2Health -= 1;
            StartCoroutine("HitDelay");
        }
        else if (collision.gameObject.CompareTag("Bullet"))
        {
            EnemySpawn.numberOfEnemies -= 1;
            TriggerDeath();
        }
	}

    //AoE Collider script, detects when the AoE is active and if the trigger collides they die.
    void OnTriggerStay(Collider AoEExp)
    {
        if (Player.aoeActive == true && (AoEExp.gameObject.CompareTag("Explosion") || AoEExp.gameObject.CompareTag("Explosion2")))
        {
            EnemySpawn.numberOfEnemies -= 1;
            TriggerDeath();
        }
    }

    //When the application quits there can be errors due to the spawning of effects when the enemies are deleted. This is used to ensure that doesn't happen.
    void OnApplicationQuit()
    {
        isQuitting = true;
    }

    //Instead of destroying this object we trigger its death which will disable it, in its parent class, and put it in the free pool.
    public override void TriggerDeath()
    {
        if (!isQuitting)
		{

            base.TriggerDeath();
            Instantiate(boom, new Vector3(transform.position.x, transform.position.y + 2, transform.position.z), Quaternion.identity);
		}
    }
	
    //Delay when player is hurt to ensure health isn't instantly reduced to zero.
    IEnumerator HitDelay()
    {
        yield return new WaitForSeconds(0.5f);
    }
}
