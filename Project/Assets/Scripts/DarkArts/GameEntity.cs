﻿using UnityEngine;
using System.Collections;

public class GameEntity : MonoBehaviour 
{
    /*
     *  Author: Richard Weeks & Edited by Luke Taylor
     *  Purpose: Base GameEntity class to be inherited throughout the game, to assist utilisation of Pooling and Entity management
     */

    protected Vector3 position;

    //Enumerated states that each GameEntity is in. Intilising on creation, Alive when in use, Dying when triger death is called.
    public enum eState
    {
        Initialising = 0,
        Alive,
        Dying
    }

    //State declaration
    protected eState    state;

    //State getter and setter
    public eState       State { get { return state; } }

	// Use this for initialization
	void Start () 
    {
	    Initialise();
	}

    // Set state to alive by default
    public virtual void Initialise()
    {
        state = eState.Alive;
    }

    //When "killed" this will be called and release the GameEntity.
    public virtual void TriggerDeath()
    {
        Release();
    }
	
    //Sets the state of Dying and releases the GameEntity, if that gameObject is null then false is returned at the gameObject is destroyed
    public void Release()
    {
        state = eState.Dying;
        if(ObjectPooler.Release(this.gameObject) == false)
        {
            GameObject.Destroy(gameObject);
        }
    }
}
