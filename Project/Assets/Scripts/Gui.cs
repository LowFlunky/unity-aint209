﻿  using UnityEngine;
using System.Collections;

public class Gui : MonoBehaviour {

    /*
     * Author: Luke Taylor
     * Purpose: Create, Update and position the GUI objects dependent on screen size.
     */ 

    // Player 1 Health to monitor, starts at 10
    [HideInInspector]
	public static int player1Health = 10;
    // Player 2 Health to monitor, starts at 10
    [HideInInspector]
	public static int player2Health = 10;
    // GUI Skin to edit Label.Text font
    [Tooltip("The Gui Skin to render our GUI text, labels, buttons etc.")]
	public GUISkin uiSkin;
    
    // Used when starting the game
    private bool displayButtonStart;
    // Used for cleanup when dead
    private bool displayButtonDead;
    // Score timer counts up as your live, potential for longest lived etc
	private int timeScore;
    // Game time used to increase time score each second.
	private float startTime;

    //Instantiate score as 0, incase of left over data, and assign the game time as startTime.
	void Start()
	{
        displayButtonStart = true;
        displayButtonDead = false;
        Time.timeScale = 0;
		timeScore = 0;
		startTime = Time.time;
        player1Health = 10;
        player2Health = 10;
	}

    //GUI creates Players health, Score, Wave counter in the assigned positions on the screen. If they players die then a popup shows they have died.
    void OnGUI()
    {
        if (displayButtonStart == true)
        {
            // Used for positioning
            int hold = (Screen.width / 2) /2;
            GUI.Label(new Rect(((Screen.width/2)/2)-100,(Screen.height/2)-100,120,100),"W,A,S,D = move, Arrows = shoot, Space = Share Health *Be close to Player 2*");
            GUI.Label(new Rect(((Screen.width / 2)+ hold ) - 100, (Screen.height / 2) - 100, 150, 100), "Left Stick = move, Y,X,A,B = shoot, Select/Start = Share Health *Be close to Player 1*");
            if (GUI.Button(new Rect((Screen.width / 2) - 50, (Screen.height/2) - 90, 90, 70), "<size=30>Play</size>"))
            {
                displayButtonStart = false;
                Player.start = true;
                Time.timeScale = 1;
            }
        }

        //Assign Gui Skin used for styles
		GUI.skin = uiSkin;
		GUI.Label (new Rect (5, 5, 800, 100), "Player 1 Health: " + player1Health);

		GUI.Label (new Rect (Screen.width / 2 ,5 ,800 ,100), "Player 2 Health: " + player2Health);

		GUI.Label (new Rect ((Screen.width / 2) - 100, Screen.height - 50, 800, 100), "Time Survived: " + timeScore);
        int waveHolder = EnemySpawn.wave;

        // Current wave changes the size and colour of the Wave text.
        if (EnemySpawn.wave < 5)
        {
            if (EnemySpawn.waveDelay == true)
            {
                GUI.Label(new Rect(Screen.width / 2 - 50, Screen.height / 2 - 200, 800, 100), "WAVE " + (waveHolder));
            }
            else
            {
                GUI.Label(new Rect(5, 65, 800, 100), "WAVE " + waveHolder);
            }
        }
        else if (EnemySpawn.wave >= 5 && EnemySpawn.wave < 10)
        {
            if (EnemySpawn.waveDelay == true)
            {
                GUI.Label(new Rect(Screen.width / 2 - 50, Screen.height / 2 - 200, 800, 100), "<size=50>WAVE " + (waveHolder) + "</size>");
            }
            else
            {
                GUI.Label(new Rect(5, 65, 800, 100), "<size=50>WAVE " + waveHolder + "</size>");
            }
        }
        else if (EnemySpawn.wave >= 10)
        {
            if (EnemySpawn.waveDelay == true)
            {
                GUI.Label(new Rect(Screen.width / 2 - 50, Screen.height / 2 - 200, 800, 100), "<size=60><color=red>WAVE " + (waveHolder) + "</color></size>");
            }
            else
            {
                GUI.Label(new Rect(5, 65, 800, 100), "<size=60><color=red>WAVE " + waveHolder + "</color></size>");
            }
        }

        if (player1Health <= 0)
        {
            GUI.Label(new Rect((Screen.width/4),(Screen.height/4),Screen.width,Screen.height), "PLAYER 1 IS DEAD");
        }

        if (player2Health <= 0)
        {
            GUI.Label(new Rect(((Screen.width / 4) * 3), (Screen.height / 4), Screen.width, Screen.height), "PLAYER 2 IS DEAD");
        }

        //If both players are dead then show the game over button.
        if (player1Health <= 0 && player2Health <= 0)
        {
            displayButtonDead = true;
            if (displayButtonDead == true)
            {
                if (GUI.Button(new Rect((Screen.width / 2 - 100), (Screen.height / 2), 200, 90), "<size=30>Game Over</size>"))
                {
                    Enemy.isQuitting = true;
                    Application.LoadLevel("MENU");
                    displayButtonDead = false;
                }
            }

        }
	}

    //Update score each second, not each frame.
	void Update()
	{
		timeScore = (int)(Time.time - startTime);

        //Force health to not drop below zero.
        if (player1Health < 0)
            player1Health = 0;
        if (player2Health < 0)
            player2Health = 0;
	}
}
