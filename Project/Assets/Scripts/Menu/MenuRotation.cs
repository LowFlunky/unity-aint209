﻿using UnityEngine;
using System.Collections;

public class MenuRotation : MonoBehaviour {

    /*
     * Author: Luke Taylor
     * Purpose: Used in the Main menu to randomly spin objects.
     */ 
	
    // Random values for spinning to be used in each prefab
    // X is used for speed of rotation
    private int x;
    // Y is used for direction of rotation
    private int y;

    // Assign a random speed and a random direction used these variables.
	void Start () {
        x = Random.Range(10, 600);
        y = Random.Range(1, 4);
	}
	
	// Use these random variables to constantly spin the objects in a random direction.
	void Update () {

        if(y <= 1)
        {
            transform.Rotate((Vector3.right * x) * Time.deltaTime);
        }
        else if (y == 2)
        {
            transform.Rotate((Vector3.left * x) * Time.deltaTime);
        }
        else if (y == 3)
        {
            transform.Rotate((Vector3.forward * x) * Time.deltaTime);
        }
        else
        {
            transform.Rotate((-Vector3.forward * x) * Time.deltaTime);
        }
	}
}
