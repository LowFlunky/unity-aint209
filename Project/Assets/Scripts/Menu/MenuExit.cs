﻿using UnityEngine;
using System.Collections;

public class MenuExit : MonoBehaviour
{
    /*
     * Author: Luke Taylor
     * Purpose: Main Menu to quit on mouse click
     */

    void OnMouseDown()
    {
        Application.Quit();
    }
}
