﻿using UnityEngine;
using System.Collections;

public class Start : MonoBehaviour {
    
   /*
    * Author: Luke Taylor
    * Purpose: Main Menu to start on mouse click
    */

    void OnMouseDown()
    {
        Application.LoadLevel("MAIN");
    }


}
