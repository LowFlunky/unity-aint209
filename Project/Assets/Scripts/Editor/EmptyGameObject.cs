﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(GameEntity))]
public class EmptyGameObject : Editor
{
    /*
     * Author:  Luke Taylor
     * Purpose: Create a new GameObject in a centered location instead of a messy random implementation that Unity allows, using hotkey Shift + Alt + M
     */ 

    [MenuItem("GameObject/Create Centered Empty GameObject #&m", false, 0)]
    static void CreateEmptyGameObject()
    {
        GameObject go = new GameObject("GameObject");
        Vector3 v3 = new Vector3(0, 0, 0);

        go.transform.position = v3;
        go.transform.localRotation = Quaternion.Euler(v3);

        Selection.activeGameObject = go;
    }

    /*[MenuItem("Custom/EditVariableWindows", false, 1)]
    private static void showEditor()
    {
        EditorWindow.GetWindow<EmptyGameObject>(false, "Select Variables");
    }

    [MenuItem("Custome/EditVariableWindows", true)]
    private static bool showEditorValidator()
    {
        return true;
    }*/

    /*public override void OnInspectorGUI()
    {
        EnemySpawn ge = (EnemySpawn)target;

        //ge.numOfEn = EditorGUILayout.IntField("Number Of Enemies", ge.numOfEn);
        EditorGUILayout.LabelField("Enemies", ge.numOfEn.ToString());
    }
    */
}
