﻿using UnityEngine;
using System.Collections;

public class JoystickControl : MonoBehaviour
{

    /*
     * Author: Luke Taylor
     * Purpose: XBOX360 Controller movement
     */

    // Player movement speed
    private float speed;

    //Speed is set on object creation
    void Start()
    {
        speed = 10.0f;
    }

    //If the Players health is above 0, use the input manager to take the Joysticks input, turning that input into force translations that then move the player 
    //by the set speed which is smooth by delta.Time
    void Update()
    {
        if (Gui.player2Health > 0)
        {
            float translation = Input.GetAxis("Left Joystick Vertical") * speed;
            float translation2 = Input.GetAxis("Left Joystick Horizontal") * speed;
            translation *= Time.deltaTime;
            translation2 *= Time.deltaTime;
            transform.Translate(0, 0, translation);
            transform.Translate(translation2, 0, 0);
        }
    }
}
