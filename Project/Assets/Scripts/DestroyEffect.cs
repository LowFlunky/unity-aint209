﻿using UnityEngine;
using System.Collections;

public class DestroyEffect : MonoBehaviour
{
    /*
     * Author: Luke Taylor
     * Purpose: Destroy this object after 2 seconds, used for destroying effects that enemies create on death.
     */

    //Used to ensure that explosions do not happen when the game is terminated and the user is taken back to the main menu
    private bool endGame;

    //Explosion sound
    [Tooltip("The sounds of an enemy explosion")]
    public AudioClip boom;

    //When Object is created, start a coroutine and play explosion Audio
    void Start()
    {
        endGame = false;
        if (endGame == false)
        {
            StartCoroutine("DestroyAfterTime");
            audio.PlayOneShot(boom, 1f);
        }
    }

    //After 2 seconds, destroy this gameObject.
    IEnumerator DestroyAfterTime()
    {
        yield return new WaitForSeconds(2);
        Destroy(gameObject);
    }

    //When the application is terminated ensure that explosions do not happen in the next scene
    void OnApplicationQuit()
    {
        endGame = true;
    }
}
	

