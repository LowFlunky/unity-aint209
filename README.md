Submission README
=========
---


Personal Details (Students Fill In Below)
===
---
- **Student Name**:  LUKE TAYLOR
- **Student Number**: 10420226


- **Course**: Computing & Games Development
- **Module**: AINT209
- **Assignment Title**: Duality


1. **Blog Link**: https://lowflunky.wordpress.com/category/unity/
2. **Screen Capture Link**: http://www.youtube.com/watch?v=Vu5eToJFegA
3. **Credits**
*  Unity Template - Interactive Systems Studio

*  Unity Standard Terrain

*  Skybox http://rockandroald.net/wordpress/mondays-are-for-free-skyboxes/

*  Explosion sounds: https://www.freesound.org/people/tommccann/sounds/235968/

*  AOE Effect: http://u3d.as/content/kakky/ky-magic-effects-free/9kx

*  Explosion Effect: https://www.assetstore.unity3d.com/en/#!/content/21245

*  Music/Effects - Freesounds.org all content under the Creative Commons 0 licence thus needing no credit, direct links can be added on request..

*  Tutorial given for creating Capsule's Textures, along with future ideas, by Paul, attempt at recreation due to lost work.

*  General assistance, feedback and ideas given by Marius.

*  Unity API website and Stack Overflow used for scripting reference and error assistance.

* RimShader: Marius Varga built alongside us in extra Shader tutorials.

* Richard Weeks: Multiple lectures and tutorials highlighting various implementations used to assist C# coding and knowledge.

---


Assignment Details
===
A Coop game utilising Keyboard and XboX controls, players fight hordes of incoming enemies of varying types that they need to work together to destroy. They score via staying alive as hitting enemies will reduce their life, enemies will get faster and stronger and there will be more in numbers as the game progresses.
2 Players hold of against waves of enemies which get harder as they progress, both players have an AoE on a cooldown of 5 seconds. Their job is to not die for as long as possible. This project is designed to be expanded into a puzzle/action adventure later with a chance of Occulus, or other inputs, being implemented.
---
###Deadline
**Monday 16th Feburary 2015.

---
###Module Documentation
[AINT 209 Module Overview](https://dle.plymouth.ac.uk/pluginfile.php/206628/mod_resource/content/1/AINT209overview2014.pdf)

---
###Submission Requirements
- You have added **iss-plymouth** as a user to your repository (see [instructions here](http://homepage.iss.io/bitbucket-add-user.html))
- There is a folder called **Submission** in the root of your repo (all of you submission files will live in here)
- Provide the following files:
    1. A pdf of your **One Page Description** 
    2. A compiled **Windows Binary (.exe)** of your *Unity Project*
    3. A *youtube* or *vimeo* link to a **Screen Capture** of your project
    4. A link to your development blog
    5. A pdf **summary feedback of your project**

--- 
###Submission Folder Layout

```
/
??? README.md
??? /Submission
    ??? Project Description.pdf
    ??? /Builds
        ??? Duality_Windows.exe
        ??? Duality_Linux.x86
        ??? Duality_Mac.app
    ??? YouTube/Vimeo Screen Capture Link
    ??? Tumblr/Development blog link
    ??? Summary Feedback Evaluation.pdf


```

---
###Assignment Briefing
Duality Prototype; A 2 player, single screen cooperative game using alternate controllers.

*Learning Outcome*: Identify and implement appropriate solutions for individual project.

**Part 1** (6 week introductory project term 1)

Duality - A co-op single screen multiplayer game developed in Unity 3D
Demo with student/staff peer review in session 6
Iterative development blog (to include minimum of weekly post/captures/embedded 
demo/solution)

GIT repository with readme evaluation (feedback from peer/staff review, Play
through screen capture and desktop build (latest iteration) refer to GIT template for 
deliverables.

**25%** Objective is to explore relevant game mechanics for different forms of co 
operative game play on a single screen, for example player one could use a leap 
motion to walk across a tight rope over a ravine while player two blows into a 
microphone to clear fog, aiding player one to proceed, revealing obstacles and 
avoiding danger etc

Provide an initial one page outline of initial concept identifying core mechanics 
and controllers used. (Add to pivotal tracker story or developer blog)

Plan a series experiments to test and iterate your core mechanics (Add 
documents to pivotal tracker stories and/or developer blog).

Implement experiments in Unity and test with peer group (document with 
screen/movie captures)

Integrate most successful experiments into a single coop game for two players 
on a single screen using two different controllers.

Provide alternate control set up for two players on a standard keyboard (as a 
backup for when external controllers un available � ie: WASD, cursor keys, 
control, option, space and escape.

**Part 2a** 
(6 week iteration of previous project, completion & documentation of C# Dark
Arts weekly exercises (developer blog) and detailed GDD using provided template.

Inclusion of additional features functionality based on previous peer/staff review and
integrating new module material (C# Dark Arts guest lecture series)
Iterative development blog and continuation of GIT repo.

LO: Research for, design of, and implementation of a substantial individual project.

LO: Identify and implement appropriate solutions for individual project.

Submission: Upload Game Design Document using provided template including links to:

GIT repo, Developer Blog, Youtube playhrough/documentation of final iteration.

**25%**